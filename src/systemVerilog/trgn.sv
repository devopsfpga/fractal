`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.03.2020 20:03:32
// Design Name: 
// Module Name: trg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// Implementation as in: https://people.csail.mit.edu/devadas/pubs/ches-fpga-random.pdf
//////////////////////////////////////////////////////////////////////////////////


module trgn(
    input clk,
    input irdv,
    output reg ordv
    );
 
 reg q1;
 reg metastable;
 reg idelay_enable = 1'b0;
 reg idelay_inc_dec = 1'b0;
 reg irdv_delay;
 integer cnt = 0;
 
 always_ff @(posedge clk)
 begin
    metastable <= !metastable;
 end
   
 always_ff @(posedge clk)
 begin
    ordv <= irdv_delay ^ metastable;
 end
   
   
(* IODELAY_GROUP = "TRNG" *) 

IDELAYE2 #(
    .CINVCTRL_SEL("FALSE"), // Enable dynamic clock inversion (FALSE, TRUE)
    .DELAY_SRC("IDATAIN"), // Delay input (IDATAIN, DATAIN)
    .HIGH_PERFORMANCE_MODE("FALSE"), // Reduced jitter ("TRUE"), Reduced power ("FALSE")
    .IDELAY_TYPE("VARIABLE"), // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
    .IDELAY_VALUE(0), // Input delay tap setting (0-31)
    .PIPE_SEL("FALSE"), // Select pipelined mode, FALSE, TRUE
    .REFCLK_FREQUENCY(200.0), // IDELAYCTRL clock input frequency in MHz (190.0-210.0).
    .SIGNAL_PATTERN("DATA") // DATA, CLOCK input signal
)
IDELAYE2_inst (
    .CNTVALUEOUT(), // 5-bit output: Counter value output
    .DATAOUT(irdv_delay), // 1-bit output: Delayed data output
    .C(clk), // 1-bit input: Clock input
    .CE(idelay_enable), // 1-bit input: Active high enable increment/decrement input
    .CINVCTRL(1'b0), // 1-bit input: Dynamic clock inversion input
    .CNTVALUEIN(5'b00000), // 5-bit input: Counter value input
    .DATAIN(1'b0), // 1-bit input: Internal delay data input
    .IDATAIN(irdv), // 1-bit input: Data input from the I/O
    .INC(idelay_inc_dec), // 1-bit input: Increment / Decrement tap delay input
    .LD(1'b0), // 1-bit input: Load IDELAY_VALUE input
    .LDPIPEEN(1'b0), // 1-bit input: Enable PIPELINE register to load data input
    .REGRST(REGRST) // 1-bit input: Active-high reset tap-delay input
);

always_ff @ (posedge clk)
begin
// monitor cnt
    if (ordv == 1'b1) cnt = cnt + 1;
    else cnt = cnt - 1;
end 

always_ff @ (posedge clk)
begin
// monitor cnt
    idelay_enable = 1'b0;
    if (cnt > 7) 
    begin 
        idelay_enable = 1'b1;
        idelay_inc_dec = 1'b0;
        cnt = 0; 
    end 
    if (cnt < -7) 
    begin
        idelay_inc_dec = 1'b1;
        cnt = 0;
    end 
end 
  
    
endmodule
