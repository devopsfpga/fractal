`timescale 1ns / 1ps

module top_level(
    input sys_clk,
    output [3:0] leds
    );

reg [25:0] x;
reg [25:0] y;    
reg clk;
reg reset;
reg locked;
reg [3:0] selector;
reg [9:0] rbits;
reg reset;
         
clk_wiz_0 clk_gen 
 (
  // Clock out ports
  clk,
  // Status and control signals
  locked,
 // Clock in ports
  sys_clk
 );         
         
always_ff @(posedge clk)
    reset = ~locked;
            

genvar i;
generate
   for (i=0; i < 10; i++) begin
    random_generator # (10) rdn (clk, rbits[i]);
   end
endgenerate


always_ff @(posedge clk)
 begin
    if (rbits < 10'd10)  
        selector <= 4'b0001;
    else if (rbits < 10'd880) 
        selector <= 4'b0010;        
    else if (rbits < 10'd952) 
        selector <= 4'b0100;
    else 
        selector <= 4'b1000;
 end
 
 vio_0 vio (clk, rbits);
 
 ila_0 ila (clk, x[25:10], y[25:10], rbits);
 
 assign leds = selector;
 
 fractal frac(reset, clk, selector, x, y);

endmodule