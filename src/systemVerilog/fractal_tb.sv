`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.03.2020 16:28:08
// Design Name: 
// Module Name: fractal_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fractal_tb ();

reg clk;
reg reset;
bit out;
wire enable;
reg [31:0] x;
reg [31:0] y;
reg [3:0] selector;
bit [31:0] selector_full;
integer f, i;

fractal frac(reset, clk, selector, x, y);

// Clock generator
  always
  begin
    #5 clk = 1;
    #5 clk = 0;
  end

  // Test stimulus
  initial
  begin
    reset = 1;
    #10; // Should be reset
    reset = 0;
    #10000;
    $finish;
  end

// generate the random inputs
always_ff @(posedge clk or posedge reset)
 if (reset) begin
   selector <= 4'b0000;
 end else begin
    selector_full = $urandom_range(0, 100);
    if (selector_full < 1)  
        selector <= 4'b0001;
    else if (selector_full < 86) 
        selector <= 4'b0010;
    else if (selector_full < 93) 
        selector <= 4'b0100;
    else 
        selector <= 4'b1000;    
 end  

    
  initial 
  begin
  f = $fopen("output.txt","w");

  @(negedge reset); //Wait for reset to be released
  for (i=0; i< 1000; i = i + 1) begin
    @(posedge clk);  
    $fwrite(f,"%f, %f\n",85.0*x, 57.0*y-275);
  end
  $fclose(f);  
end


endmodule
