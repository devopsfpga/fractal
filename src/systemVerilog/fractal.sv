`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.03.2020 18:01:12
// Design Name: 
// Module Name: fractal
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fractal(
    input reset, 
    input clk,
    input [3:0] selector,
    output reg [25:0] x,
    output reg [25:0] y 
    );
     
 always_ff @(posedge clk or posedge reset)
 if (reset) begin
   x <= 26'd0;
   y <= 26'd0;
 end else begin
   case (selector)
      4'b0001  : begin
        x = 26'd0;
        // y <= 0.16 * y; => ((y * 163) / 1024 ) * 1024;
        y = (y * 26'd163); 
       end
      4'b0010  : begin 
        //x <= 0.85 * x + 0.04 * y;
        // =>  (((870 * x) + (41 * y)) / 1024) * 1024; 
        x = ((x * 16'd100) + (x * 16'd41)); 
        //y <= -0.04 * x + 0.85 * y + 1.6;
        // => ((((-41 * x) + (870 * y)) * 2 + 3) / 2) / 1024) * 1024;
        y = ((((16'd870 * y) - (16'd41 * y)) << 1) + 16'd3) >> 1;   
      end
      4'b0100  : begin
        //x = 0.20 * x - 0.26 * y;
        // =>  (((205 * x) - (226 * y)) / 1024) * 1024;
        x = ((16'd205 * x) - (16'd226 * y));
        //y = 0.23 * x + 0.22 * y + 1.6;     
        // => (((235 * x) + (225 * y))*2 + 3) / 2;
        y = (((16'd235 * x) + (16'd225 * y)) << 1 + 3) >> 1;
        end
      4'b1000  :  begin
        //x = -0.15 * x + 0.28 * y;
        // => ((287 * y) / 1024 - (154*x)) / 1024) * 1024;
        x = (16'd287 * y) - (16'd154 * x); 
        //y = 0.26 * x + 0.24 * y + 0.44;
        // => (((266 * x) + (245*y)) * 2 + 1) / 2; 
        y = (((16'd266 * x) + (16'd245 * y)) << 1 + 1) >> 1; 
      end
      default : begin 
        x <= x;
        y <= y;
      end 
    endcase 
 end
   
endmodule
