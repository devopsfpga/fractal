import turtle
import random

pen = turtle.Turtle()
pen.speed(10)
pen.color('green')
pen.penup()


def pure_python():
    x = 0
    y = 0
    for n in range(100000):
        pen.goto(85*x, 57*y-275)  # 57 is to scale the fern and -275 is to start the drawing from the bottom.
        pen.pendown()
        pen.dot()
        pen.penup()
        r = random.random()  # to get probability
        r = r * 100
        xn = x
        yn = y
        if r < 1:  # elif ladder based on the probability
            x = 0
            y = 0.16 * yn
        elif r < 86:
            x = 0.85 * xn + 0.04 * yn
            y = -0.04 * xn + 0.85 * yn + 1.6
        elif r < 93:
            x = 0.20 * xn - 0.26 * yn
            y = 0.23 * xn + 0.22 * yn + 1.6
        else:
            x = -0.15 * xn + 0.28 * yn
            y = 0.26 * xn + 0.24 * yn + 0.44

def read_vivado():
    with open('iladata.csv', 'r') as f:
        for line in f:        
            x_s,y_s = line.split(',')
            print (x_s)
            print (y_s)
            pen.goto(float(x_s)/128, float(y_s)/128)
            pen.pendown()
            pen.dot()
            pen.penup()
        
read_vivado()
#pure_python()
input("")