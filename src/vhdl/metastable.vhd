library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VComponents.all;

entity metastable is
    Port ( clk : in STD_LOGIC;
           meta : out STD_LOGIC);
end metastable;

architecture Behavioral of metastable is

signal meta_bits : std_logic_vector(2 downto 0);
signal meta_bit_d : std_Logic;
signal rbit_i : std_logic;

attribute dont_touch : string;
attribute dont_touch of meta_bits : signal is "true";

begin

process (meta_bits)
begin
    meta_bits(1) <= not meta_bits(0);
    meta_bits(2) <= not meta_bits(1);
    meta_bits(0) <= not meta_bits(2); 
end process;

meta_bit_d <= meta_bits(2);

process (clk)
begin
    if (rising_edge(clk)) then
        rbit_i <= meta_bit_d;
    end if;
end process;

meta <= rbit_i;

end Behavioral;
