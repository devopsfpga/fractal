----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.03.2020 19:24:35
-- Design Name: 
-- Module Name: random_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity random_generator is
    generic (NUM_RINGS : integer := 3);
    Port ( clk : in STD_LOGIC;
           rbit : out STD_LOGIC);
end random_generator;

architecture Behavioral of random_generator is

component metastable is
    Port ( clk : in STD_LOGIC;
           meta : out STD_LOGIC);
end component metastable;

signal metas : std_logic_vector(NUM_RINGS-1 downto 0);
signal rbit_i : std_logic;

 function XOR_REDUCE(ARG: STD_LOGIC_VECTOR) return UX01 is
	-- pragma subpgm_id 403
	variable result: STD_LOGIC;
    begin
	result := '0';
	for i in ARG'range loop
	    result := result xor ARG(i);
	end loop;
        return result;
    end;

begin

g_rings: for I in 0 to NUM_RINGS-1 generate
    m: metastable
    port map (clk => clk, 
              meta => metas(I));
              
    rbit_i <= rbit_i xor metas(I);      

end generate;

rbit <= XOR_REDUCE(metas);

end Behavioral;
